INSERT INTO public.oauth_client_details (client_id,resource_ids,client_secret,"scope",authorized_grant_types,web_server_redirect_uri,authorities,access_token_validity,refresh_token_validity,additional_information,autoapprove) VALUES
('workflow-api-client','workflow-api','$2a$10$fiIAxsnEuyx/HmhwH2T6qO1EdWXmsMNUHxf/0..apuM/p1YRbIjiu','READ,WRITE,MANAGE','refresh_token,authorization_code,password,client_credentials','http://localhost:5000','READ,WRITE',3600,600,NULL,'false');

INSERT INTO public.sys_user (email,"password",username,account_non_expired,account_non_locked,credentials_non_expired,enabled) VALUES
('guest@workflow.sys', '$2a$10$25bIL7PAlCkmo9TyFMdc9.ItQ3hCKwQ334TfUlUYg2plwoGi5IMYO','guest',true,true,true,1)
,('developer@workflow.sys', '$2a$10$WabZLNnQSX/S4EE7RnxTvuXSeOAKNht0e2AIx20LI9IPqGO6dqNBq','developer',true,true,true,1)
,('admin@workflow.sys', '$2a$10$dRUkdlji.MXSlT0lKi27xOMdbGdpeQYwJXTJLQjzyIvvLzhatfqle','admin',true,true,true,1);

INSERT INTO public.sys_role (id,"name") VALUES
(1,'ROLE_ADMIN')
,(2,'ROLE_MANAGER')
,(3,'ROLE_USER')
,(4,'ROLE_DEVELOPER');

INSERT INTO public.sys_permission ("name") VALUES
('CREATE')
,('READ')
,('UPDATE')
,('DELETE');

INSERT INTO public.sys_user_role (user_id,role_id) VALUES
(3,1)
,(2,4)
,(1,3);

