package com.asiregar310.demo.securerestapi.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    @GetMapping("/secured")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String secured() {
        return "secured";
    }
}
