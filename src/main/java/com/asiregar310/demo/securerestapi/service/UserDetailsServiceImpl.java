package com.asiregar310.demo.securerestapi.service;

import com.asiregar310.demo.securerestapi.dao.UserDetailsDao;
import com.asiregar310.demo.securerestapi.model.AuthUserDetail;
import com.asiregar310.demo.securerestapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserDetailsDao userDetailsDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> optionalUser = userDetailsDao.findByUsername(s);
        optionalUser.orElseThrow(() -> new UsernameNotFoundException("Incorrect username or password."));

        UserDetails userDetails = new AuthUserDetail(optionalUser.get());

        // check user status. invalid status will raise an exception
        new AccountStatusUserDetailsChecker().check(userDetails);

        return userDetails;
    }
}
