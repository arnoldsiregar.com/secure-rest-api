package com.asiregar310.demo.securerestapi.dao;

import com.asiregar310.demo.securerestapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDetailsDao extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
