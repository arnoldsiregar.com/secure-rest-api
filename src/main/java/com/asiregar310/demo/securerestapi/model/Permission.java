package com.asiregar310.demo.securerestapi.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "sys_permission")
@Data
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "name")
    private String name;
}